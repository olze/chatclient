package chatclient.gui;

import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import java.util.concurrent.FutureTask;

public class TestPrivChatWindow {

    Stage dialog;
    WebView privateChatWebView;

    public TestPrivChatWindow(Stage parent) {

        runInFxThread(() -> {
            dialog = new Stage();
            privateChatWebView = new WebView();
            privateChatWebView.getEngine().setJavaScriptEnabled(true);
        });
        dialog.initOwner(parent);
        final VBox dialogVbox = new VBox(20);
        dialogVbox.getChildren().add(privateChatWebView);
        final Scene dialogScene = new Scene(dialogVbox, 300, 200);
        runInFxThread(() -> dialog.setScene(dialogScene));

    }

    public void addText(String s) {
        Runnable runnable = () -> {
            privateChatWebView.getEngine().reload();
            privateChatWebView.getEngine().executeScript("appendText(\"" + s + "\");");
        };

        runInFxThread(runnable);
    }

    public void init() {
        final String init = """
                <html>
                  <head>
                    <script language="JavaScript">function appendText(extraStr) {
                      document.getElementsByTagName('body')[0].innerHTML = document.getElementsByTagName('body')[0].innerHTML + extraStr;
                    }</script>
                  </head>
                  <body>
                  </body>
                </html>
                """;
        runInFxThread(() -> privateChatWebView.getEngine().loadContent(init));
        runInFxThread(() -> privateChatWebView.getEngine().executeScript("window.scrollTo(0, document.body.scrollHeight);"));

    }

    static void runInFxThread(Runnable runnable) {

        try {
            if (Platform.isFxApplicationThread()) {
                runnable.run();
            } else {
                FutureTask<Object> futureTask = new FutureTask<>(runnable,
                        null);
                Platform.runLater(futureTask);
                futureTask.get();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
