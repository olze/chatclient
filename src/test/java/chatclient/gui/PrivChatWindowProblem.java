package chatclient.gui;

import javafx.stage.Stage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.testfx.framework.junit5.ApplicationExtension;
import org.testfx.framework.junit5.Start;

@ExtendWith(ApplicationExtension.class)
public class PrivChatWindowProblem {

    private Stage stage;

    @Start
    private void start(Stage stage) {
        this.stage = stage;
    }

    @Test
    void test() {
            TestPrivChatWindow privChatWindow = new TestPrivChatWindow(stage);
            privChatWindow.init();
            privChatWindow.addText("hi");
    }

}
